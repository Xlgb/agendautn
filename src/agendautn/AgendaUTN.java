/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendautn;

import agenda.datos.Actividad;
import agenda.datos.Contacto;
import agenda.datos.Usuario;
import agenda.logica.ManejoActividad;
import agenda.logica.ManejoContacto;
import agenda.logica.ManejoUsuario;
import agenda.util.Util;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author dell
 */
public class AgendaUTN {
    static ManejoUsuario logUser = new ManejoUsuario();
    static ManejoContacto logContact = new ManejoContacto();
    static ManejoActividad logActivi = new ManejoActividad(); 
    
   
    public static void main(String[] args) {
        String menu1 = "          Agenda - UTN v0.1\n"
            + "          *******Menu******* \n"
            + "1.Registar Usuario\n"
            + "2.Iniciar Sesión\n"
            + "3.Restablecer Contraseña\n"
            + "4.Salir\n";
        
        String menu2 = "          Agenda - UTN v0.1\n"
                + "1.Contactos\n"
                + "2.Actividades\n"
                + "3.Notificaciones\n"
                + "4.Cerrar Sección\n";
        String menuContactos = "          Agenda - UTN v0.1\n"
                + "          *******Contacto******* \n"
                + "1.Registrar Contacto\n"
                + "2.Editar Contacto\n"
                + "3.Eliminar Contacto\n"
                + "4.Ver Contactos\n"
                + "5.Volver\n";
        String menuActividades= "          Agenda - UTN v0.1\n"
                + "          *******Actividades*******\n"
                + "1.Registrar Actividades\n"
                + "2.Editar Actividades\n"
                + "3.Ver Actividades\n"
                + "4.Eliminar Actividades\n"
                + "5.Volver\n";
        
        String menuNotificaciones= "          Agenda - UTN v0.1\n"
                + "          *******Notificaciones*******\n"
                + "1.Anuales\n"
                + "2.Semanales";
        
        
        String salir = "¿Está seguro que desea salir?";
        
        OUTAPP:
        while (true) {
            int op = Util.leerInt(menu1);
            OUTSECT:
            switch (op) {
                case 1:
                    String nombre = Util.leerTexto("Ingrese su nombre");
                    String correo = Util.leerTexto("Ingrese su correo");
                    String cedula = Util.leerTexto("Ingrese su numero de cedula");
                    Date fechaNacimiento = Util.leerFecha("Ingrese su Fecha de Nacimiento (dd/MM/yyyy)");
                    String passWord = Util.leerTexto("Ingrese su Contraseña");
                    String userName = Util.leerTexto("Ingrese su Nombre de Usuario");//validar si ya exite nombre user
                    String telefono = Util.leerTexto("Ingrese su Numero de Telefono");
                    boolean verificarUsuario = logUser.verificarUsuario(userName, cedula);
                    if(verificarUsuario != false && verificarUsuario != false ){
                        Util.mostrar("El Usuario ya existe!! Vuelva a intentar");
                    }else{
                    Usuario temp = new Usuario ( nombre, correo, cedula,fechaNacimiento,passWord,userName,telefono);
                    logUser.guardarUsuario(temp);
                    Util.mostrar("El Usuario se Agregó con Exito");
                    }
                    
                    break;
                case 2:
                    userName = Util.leerTexto("Ingrese su Nombre de Usuario");
                    passWord = Util.leerTexto("Ingrese su Contraseña");
                    boolean verificar = logUser.verificarAcceso(userName, passWord);
                    Usuario userLogiado = logUser.buscarNombreUsuario(userName);
                    if(verificar != true && verificar != true){
                        Util.mostrar("El Nombre de Usuario o Contraseña son Incorrectos");
                        break OUTSECT;
                         
                    }
                   
                    while (true) {
                        int opInicioSe = Util.leerInt(menu2);
                        OUTCONCT:
                        switch (opInicioSe) {
                            case 1:
                                while(true){
                                int opConctactos = Util.leerInt(menuContactos);
                                    switch (opConctactos) {
                                        case 1:
                                            cedula = Util.leerTexto("Ingrese Cedula");
                                            nombre = Util.leerTexto("Ingrese Nombre");
                                            fechaNacimiento = Util.leerFecha("Ingrese su Fecha de Nacimiento (dd/MM/yyyy)");
                                            correo = Util.leerTexto("Ingrese Correo");
                                            telefono = Util.leerTexto("Ingrese Telefono");
                                            String descripcion = Util.leerTexto("Ingrese descripcion");
                                            String cedulaUser = userLogiado.getCedula();
                                            Contacto tempo = new Contacto(cedula,nombre,fechaNacimiento,correo,telefono,descripcion,cedulaUser);
                                            logContact.guardarContacto(tempo);
                                            Util.mostrar("El Contacto se Agregó con Exito");
                                            break;
                                        case 2:
                                            String cedulaUsuario = userLogiado.getCedula();
                                            Contacto[] listaContacto = logContact.leerContacto(cedulaUsuario);

                                            int numContacto = Util.leerInt("Contactos:\n"
                                            + logContact.impContacto(cedulaUsuario, listaContacto)
                                            + "\nSeleccione un Contactos");
                                            
                                            
                                            if(numContacto<=listaContacto.length){
                                             Contacto clienEdi = listaContacto[numContacto - 1];
                                            String contactoViejo = clienEdi.getInfoContacto();
                                            String cedulaFila = clienEdi.getCedulaUser();
                                            if(cedulaUsuario.equals(cedulaFila)){
                                            cedula = Util.leerTexto("Ingrese Cedula",clienEdi.getCedula());
                                            nombre = Util.leerTexto("Ingrese Nombre",clienEdi.getNombre());
                                            fechaNacimiento = Util.leerFecha("Ingrese su Fecha de Nacimiento (dd/MM/yyyy)",clienEdi.getFechaNacimientoString());
                                            correo = Util.leerTexto("Ingrese Correo",clienEdi.getCorreo());
                                            telefono = Util.leerTexto("Ingrese Telefono",clienEdi.getTelefono());
                                             descripcion = Util.leerTexto("Ingrese descripcion",clienEdi.getDescripcion());
                                            Contacto contactoEditado = new Contacto(cedula,nombre,fechaNacimiento,correo,telefono,descripcion,cedulaUsuario);
                                            logContact.editarContacto(contactoViejo, contactoEditado);
                                            listaContacto = logContact.leerContacto(cedulaUsuario);
                                            Util.mostrar(logContact.impContacto(cedulaUsuario, listaContacto));
                                            }else{
                                                 Util.mostrar("Opción Incorrecta!!");
                                            }
                                            }else{
                                                Util.mostrar("Opción Incorrecta!!");
                                            }
                                            
                                            break;
                                        case 3:
                                             cedulaUsuario = userLogiado.getCedula();
                                             listaContacto = logContact.leerContacto(cedulaUsuario);

                                             numContacto = Util.leerInt("Contactos:\n"
                                            + logContact.imprimirContactos(cedulaUsuario, listaContacto)
                                            + "\nSeleccione un Contactos");
                                             
                                             if(numContacto<=listaContacto.length){
                                            Contacto clienEdi = listaContacto[numContacto - 1];
                                            String cedulaFila = clienEdi.getCedulaUser();
                                            if(cedulaUsuario.equals(cedulaFila)){
                                            String contactoViejo = clienEdi.getInfoContacto();
                                            logContact.eliminarContacto(contactoViejo);
                                            
                                            listaContacto = logContact.leerContacto(cedulaUsuario);
                                            Util.mostrar(logContact.imprimirContactos(cedulaUsuario, listaContacto));
                                            }else{
                                                Util.mostrar("Opción Incorrecta!!");
                                            }
                                             }else{
                                                 Util.mostrar("Opción Incorrecta!!");
                                             }
                                           
                                            break;
                                        case 4:
                                            cedulaUsuario = userLogiado.getCedula();
                                             listaContacto = logContact.leerContacto(cedulaUsuario);
                                            Util.mostrar(logContact.impContacto(cedulaUsuario, listaContacto));
                                            break;
                                        case 5:
                                            break OUTCONCT;
                                        default:
                                            Util.mostrar("Opción Incorrecta !!");
                                    }
                                }
                                
                            case 2:
                                String cedulaUsuario = userLogiado.getCedula();
                                Actividad[] listaActividad = logActivi.leerActividad(cedulaUsuario);
                                cedulaUsuario = userLogiado.getCedula();
                                listaActividad = logActivi.leerActividad(cedulaUsuario);
                                Util.mostrar("    Notificaciones\n\n"+logActivi.impActividadMuestra(cedulaUsuario, listaActividad));
                                OUTACTIV:
                                 while(true){
                                int opActividades = Util.leerInt(menuActividades);
                                    switch (opActividades) {
                                        case 1:
                                            String tipoActividad = Util.leerTexto("Ingrese Ingrese una Actividad");
                                            telefono = Util.leerTexto("Ingrese Telefono");
                                            correo = Util.leerTexto("Ingrese Correo");
                                            Date fechaActividad = Util.leerFecha("Ingrese la facha de la Actividad");
                                            String fecha = "";
                                            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                                            fecha = formato.format(fechaActividad);
                                            boolean verificarDiaFestivo = logActivi.verificarDiaFestivo(fecha);
                                            if(verificarDiaFestivo==true){
                                               boolean confirmar = Util.confirmar("Dia Feriado!!\n"
                                                        + "Deseas Continuar ?");
                                               if(confirmar == false){
                                                   break;
                                               }
                                            }
                                           
                                            String hora = Util.leerTexto("Ingrese la hora");
                                            boolean verificarHora = logActivi.verificarHoraActividad(hora);
                                            if(verificarHora != false ){
                                                Util.mostrar("Hora Ocupada!! Intente nuevamente");
                                                break ;
                                            }
                                            String cedulaUser = userLogiado.getCedula();
                                            boolean contactoRelacionado = Util.confirmar("¿Quieres relacionar un contacto?");
                                            String cedulaContacto = "";

                                            if(contactoRelacionado==true){
                                            
                                            Contacto[] listaContacto = logContact.leerContacto(cedulaUsuario);

                                            int numContacto = Util.leerInt("Contactos:\n"
                                            + logContact.impContacto(cedulaUsuario, listaContacto)
                                            + "\nSeleccione un Contactos");
                                            
                                            Contacto clienEdi = listaContacto[numContacto - 1];
                                            cedulaContacto = clienEdi.getCedula();
                                            }else{
                                            cedulaContacto = "No tiene contacto asociado";
                                            }
                                            Actividad nuevaActividad = new Actividad(tipoActividad,telefono,correo,fechaActividad,hora,cedulaUser,cedulaContacto);
                                            logActivi.guardarActividad(nuevaActividad);
                                            Util.mostrar("El Contacto se Agregó con Exito");
                                            
                                            
                                            break;
                                        case 2:
                                           
                                            int numActividad = Util.leerInt("Actividades:\n"
                                            + logActivi.impActividadMuestra(cedulaUsuario,listaActividad)
                                            + "\nSeleccione un Actividad");
                                            
                                            

                                            
                                            if(numActividad<=listaActividad.length){
                                                Actividad editarActividad = listaActividad[numActividad - 1];
                                            String cedulaFila = editarActividad.getCedulaUsuario();
                                            if(cedulaUsuario.equals(cedulaFila)){
                                            String ActividadViejo = editarActividad.getInfoActividad();
                                            tipoActividad = Util.leerTexto("Ingrese Ingrese una Actividad",editarActividad.getTipoActividad());
                                            telefono = Util.leerTexto("Ingrese Telefono",editarActividad.getTelefono());
                                            correo = Util.leerTexto("Ingrese Correo",editarActividad.getCorreo());
                                            fechaActividad = Util.leerFecha("Ingrese la facha de la Actividad",editarActividad.getFechaActividadString()); 
                                            hora = Util.leerTexto("Ingrese la hora",editarActividad.getHora());
                                            cedulaUser = userLogiado.getCedula();
                                             
                                            Actividad actividadEditado = new Actividad(tipoActividad,telefono,correo,fechaActividad,hora,cedulaUser,editarActividad.getCedulaContacto());
                                            logActivi.editarActividad(ActividadViejo, actividadEditado);
                                            listaActividad = logActivi.leerActividad(cedulaUsuario);
                                            Util.mostrar(logActivi.impActividadMuestra(cedulaUsuario, listaActividad));
                                            }else{
                                                Util.mostrar("Opción Incorrecta!!");
                                            }
                                            }else{
                                                Util.mostrar("Opción Incorrecta!!");
                                            }
                                            
                                            
                                            
                                            break;
                                        case 3:
                                            cedulaUsuario = userLogiado.getCedula();
                                            listaActividad = logActivi.leerActividad(cedulaUsuario);
                                            Util.mostrar(logActivi.impActividadMuestra(cedulaUsuario, listaActividad));
                                            break;
                                        case 4:
                                            cedulaUsuario = userLogiado.getCedula();
                                            listaActividad = logActivi.leerActividad(cedulaUsuario);

                                             numActividad = Util.leerInt("Actividad:\n"
                                            + logActivi.impActividadMuestra(cedulaUsuario, listaActividad)
                                            + "\nSeleccione un Actividad");
                                            
                                            if(numActividad<=listaActividad.length){
                                            Actividad editActividad = listaActividad[numActividad - 1];
                                            String cedulaFila = editActividad.getCedulaUsuario();
                                            if(cedulaUsuario.equals(cedulaFila)){
                                            String actividadVieja = editActividad.getInfoActividad();
                                            logActivi.eliminarActividad(actividadVieja);
                                            
                                            listaActividad = logActivi.leerActividad(cedulaUsuario);
                                            Util.mostrar(logActivi.impActividadMuestra(cedulaUsuario, listaActividad));
                                            }else{
                                                Util.mostrar("Opción Incorrecta!!");
                                            }
                                            }else{
                                                Util.mostrar("Opción Incorrecta!!");
                                            }
                                            break;
                                        case 5:
                                            break OUTCONCT;
                                        default:
                                            Util.mostrar("Opción Incorrecta !!");
                                    }
                                 }
                            case 3:
                                while (true) {   
                                     op = Util.leerInt(menuNotificaciones);
                                    switch (op) {
                                        case 1:
                                            cedulaUsuario = userLogiado.getCedula();
                                            listaActividad = logActivi.leerActividad(cedulaUsuario);
                                            Util.mostrar(logActivi.impActividadMuestra(cedulaUsuario, listaActividad));
                                            break;
                                        case 2:
                                            cedulaUsuario = userLogiado.getCedula();
                                            listaActividad = logActivi.leerActividad(cedulaUsuario);
                                            Util.mostrar(logActivi.impActividadMuestra(cedulaUsuario, listaActividad));
                                            break;
                                        default:
                                            Util.mostrar("Opción Incorrecta !!");
                                    }
                                }
                            case 4:
                                break OUTSECT;
                            default:
                                 Util.mostrar("Opción Incorrecta !!");
                        }
                        
                    }
                    
                    
                case 3:
                    String ced = Util.leerTexto("Digite la cédula del dueño");
                    Usuario arrCli = logUser.buscarUsuario(ced);
                    if(arrCli == null){
                        Util.mostrar("Usuario no existe");
                        break;
                    }
                    
                    String nombreCon = Util.leerTexto("Nombre");
                    String fechaCon = Util.leerTexto("Fecha Nacimiento");
                    
                    if(arrCli.getNombre().equals(nombreCon) && arrCli.getFechaNacimientoString().equals(fechaCon)){
                        String passUser = logUser.passRandom();
                        String info = arrCli.getInfoUsuario();
                        arrCli.setPassword(passUser);
                        logUser.editarUsuario(info, arrCli);
                        Util.mostrar(logUser.impUsuario(ced));
                    } else {
                        Util.mostrar("Respuestas inválidas!!");
                    }
                    
                     
                    break;
                case 4:
                   if (Util.confirmar(salir)) {
                        break  OUTAPP;
                   }    
                default:
                    Util.mostrar("Opción Incorrecta !!");
            }
        }
    }
}
   
    