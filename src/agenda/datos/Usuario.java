/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda.datos;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author dell
 */
public class Usuario {
    private String nombre;
    private String correo;
    private String cedula;
    private Date fechaNacimiento;
    private String password;
    private String userName;
    private String phone;
    
    //Constructor
    public Usuario(String nombre,String correo, String cedula, Date fechaNacimiento, String password, String userName, String phone){
        this.nombre = nombre;
        this.correo = correo;
        this.cedula = cedula;   
        this.fechaNacimiento = fechaNacimiento;
        this.password = password;
        this.userName = userName;
        this.phone = phone;
    }

    public Usuario() {
       
    }
    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the correo
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * @param correo the correo to set
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the fechaNacimiento
     */
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }
    
    public String getFechaNacimientoString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(fechaNacimiento);
    }

    /**
     * @param fechaNacimiento the fechaNacimiento to set
     */
    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getInfoUsuario() {
        String temp = "%s,%s,%s,%s,%s,%s,%s";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return String.format(temp,  nombre, correo,  cedula,  getFechaNacimientoString(),  password,  userName,  phone);
    }

    @Override
    public String toString() {
        return "Usuario{" + "nombre=" + nombre + ", correo=" + correo + ", cedula=" + cedula + ", fechaNacimiento=" + fechaNacimiento + ", password=" + password + ", userName=" + userName + ", phone=" + phone + '}';
    }
    
}
 

