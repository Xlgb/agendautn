/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda.datos;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
     *
 * @author dell
 */
public class Contacto {
   
    private String nombre;
    private String cedula;
    private Date fechaNacimiento;
    private String correo;
    private String telefono;
    private String descripcion;
    private String cedulaUser;
    
    public Contacto(){
    
    }

    public Contacto(String cedula, String nombre, Date fechaNacimiento, String correo, String telefono, String descripcion, String cedulaUser) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.correo = correo;
        this.telefono = telefono;
        this.descripcion = descripcion;
        this.cedulaUser = cedulaUser;
    }
   
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
    
    

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
     public String getFechaNacimientoString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(fechaNacimiento);
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCedulaUser() {
        return cedulaUser;
    }

    public void setCedulaUser(String cedulaUser) {
        this.cedulaUser = cedulaUser;
    }
    
    public String getInfoContacto() {
        String temp = "%s,%s,%s,%s,%s,%s,%s";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return String.format(temp,cedula,nombre, getFechaNacimientoString(),correo,telefono,descripcion,cedulaUser);
    }

    @Override
    public String toString() {
        return "Contacto{" + "nombre=" + nombre + ", cedula=" + cedula + ", fechaNacimiento=" + fechaNacimiento + ", correo=" + correo + ", telefono=" + telefono + ", descripcion=" + descripcion + ", cedulaUser=" + cedulaUser + '}';
    }

    
   

    
}

    

    
    
    
    