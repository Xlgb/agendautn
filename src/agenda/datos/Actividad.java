/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda.datos;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author dell
 */
public class Actividad {
    private String tipoActividad;
    private String telefono;
    private String correo;
    private Date fechaActividad;
    private String hora;
    private String cedulaUsuario;
    private String cedulaContacto;

    public Actividad() {
    }

    public Actividad(String tipoActividad, String telefono, String correo, Date fechaActividad, String hora, String cedulaUsuario, String cedulaContacto) {
        this.tipoActividad = tipoActividad;
        this.telefono = telefono;
        this.correo = correo;
        this.fechaActividad = fechaActividad;
        this.hora = hora;
        this.cedulaUsuario = cedulaUsuario;
        this.cedulaContacto = cedulaContacto;
    }

    public String getTipoActividad() {
        return tipoActividad;
    }

    public void setTipoActividad(String tipoActividad) {
        this.tipoActividad = tipoActividad;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Date getFechaActividad() {
        return fechaActividad;
    }

    public void setFechaActividad(Date fechaActividad) {
        this.fechaActividad = fechaActividad;
    }
    public String getFechaActividadString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(fechaActividad);
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getCedulaUsuario() {
        return cedulaUsuario;
    }
    

    public void setCedulaUsuario(String cedulaUsuario) {
        this.cedulaUsuario = cedulaUsuario;
    }

    public String getCedulaContacto() {
        return cedulaContacto;
    }

    public void setCedulaContacto(String cedulaContacto) {
        this.cedulaContacto = cedulaContacto;
    }

   


  
    public String getInfoActividad() {
        String temp = "%s,%s,%s,%s,%s,%s,%s";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return String.format(temp,tipoActividad,telefono,correo,getFechaActividadString(),hora,cedulaUsuario,cedulaContacto);
    }

    @Override
    public String toString() {
        return "Actividad{" + "tipoActividad=" + tipoActividad + ", telefono=" + telefono + ", correo=" + correo + ", fechaActividad=" + fechaActividad + ", hora=" + hora + ", cedulaUsuario=" + cedulaUsuario + ", cedulaContacto=" + cedulaContacto + '}';
    }

   
 
    
}
