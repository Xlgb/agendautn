/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda.logica;

import agenda.dao.ArchivoDao;
import agenda.datos.Actividad;
import agenda.datos.Contacto;
import agenda.datos.Usuario;
import agenda.util.Util;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.CompareGenerator;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author dell
 */
public class ManejoActividad {
    
    ArchivoDao ma = new ArchivoDao();
    private static final String RUTA_ACTIVID = "datos/Actividades.txt";

    /**
     * Escribe el contacto nuevo en la ruta del archivo
     *
     * @param nueva Contacto nuevo
     */
    public void guardarActividad(Actividad nueva) {
        ma.escribir(RUTA_ACTIVID, nueva.getInfoActividad());
    }

    /**
     * lee todos los Usuarios del archivo
     *
     * @param rutaAchivo La ruta del donde se encuentra el archivo
     * @return La cantidad de usuarios que hay en el archivo
     */
    public Actividad[] leerActividad(String rutaAchivo) {
        String[] actividades = ma.leer(RUTA_ACTIVID).split("\n");
        Actividad[] arr = new Actividad[actividades.length];
        for (int i = 0; i < actividades.length; i++) {
            String[] datoActividad = actividades[i].split(",");
            Actividad actividad = new Actividad();
            actividad.setTipoActividad(datoActividad[0]);
            actividad.setTelefono(datoActividad[1]);
            actividad.setCorreo(datoActividad[2]);
            actividad.setFechaActividad(Util.convertirFecha(datoActividad[3]));
            actividad.setHora(datoActividad[4]);
            actividad.setCedulaUsuario(datoActividad[5]);
            actividad.setCedulaContacto(datoActividad[6]);
            

            arr[i] = actividad;

        }
        return arr;
    }

    /**
     * Recorre la lista de Contacto para saber si ese Contacto existe y si no
     * los escribe en el archivo
     *
     * @param listaContacto Lista de Contacto nueva
     */
    public void guardarActividad(Usuario[] listaActividad) {

        for (int i = 0; listaActividad.length < 10; i++) {
            guardarActividad(listaActividad);
        }
    }

    /**
     * Verifica Usuario por numero de cedula para saber su información
     *
     * @param ced Cedula digitada por el usuario
     * @return Informacion del Usuario
     */
    public String impActividad(String ced) {
        Actividad[] listaActividad = leerActividad(RUTA_ACTIVID);
        Actividad verificarActividad = new Actividad();
        String msj = "";
        for (int i = 0; i < listaActividad.length; i++) {
            verificarActividad = listaActividad[i];
            String cedulaUser = verificarActividad.getCedulaUsuario();
            if (cedulaUser.equals(ced)) {
                String formato = " %d. %s : %s %s %s %s %s \n";
                Actividad actividadInfoNueva = listaActividad[i];

                msj = String.format(formato, (i + 1),
                        "Tipo de Actividad: " + actividadInfoNueva.getTipoActividad(), "\n" + "Telefono: " + actividadInfoNueva.getTelefono()+ "\n",  "correo: " + actividadInfoNueva.getCorreo() + "\n" + "\n", "\n", "Fecha actividad: " + actividadInfoNueva.getFechaActividadString(),
                        "Hora: " + actividadInfoNueva.getHora()+ "\n", actividadInfoNueva.getCedulaContacto());
            }
        }
        return msj;

    }
       /**
        * 
        * @param ced
        * @param listaActividad
        * @return 
        */
    public String impActividad(String ced, Actividad[] listaActividad) {
        Actividad verificarActividad = new Actividad();
        String msj = "";
        for (int i = 0; i < listaActividad.length; i++) {
            verificarActividad = listaActividad[i];
            String cedulaUser = verificarActividad.getCedulaUsuario();
            if (cedulaUser.equals(ced)) {
                String formato = " %d. %s %s %s %s %s %s %s \n";
                Actividad  actividadInfoNueva = listaActividad[i];

                 msj = String.format(formato, (i + 1),
                        "Tipo de Actividad: " + actividadInfoNueva.getTipoActividad(), "\n" , "Telefono: " + actividadInfoNueva.getTelefono()+ "\n",  "correo: " + actividadInfoNueva.getCorreo() + "\n" , "Fecha actividad: " + actividadInfoNueva.getFechaActividadString(),
                        "Hora: " + actividadInfoNueva.getHora(), "\n"+ actividadInfoNueva.getCedulaUsuario(), actividadInfoNueva.getCedulaContacto());

            }
        }
        return msj;

    }
    
    /**
     * muestra Las actividades de cada Usuario
     * @param ced cedula
     * @param listaActividad
     * @return 
     */
     public String impActividadMuestra(String ced, Actividad[] listaActividad) {
        Actividad verificarActividades = new Actividad();
        String msj = "";
        for (int i = 0; i < listaActividad.length; i++) {
            verificarActividades = listaActividad[i];
            String cedulaUser = verificarActividades.getCedulaUsuario();
            if (cedulaUser.equals(ced)) {
                String formato = " %d#\n %s %s %s\n";
                String formato1 = " %d#";
                Actividad actividadInfoNueva  = listaActividad[i];
                msj += String.format(formato, (i + 1),"Tipo de actividad: " + actividadInfoNueva.getTipoActividad()+"\n", " hora: " + actividadInfoNueva.getHora()+"\n" , "Fecha actividad:"+ actividadInfoNueva.getFechaActividadString());
            }
        }
        return msj;

    }
    
    
    
    

    /**
     * Imprime todos las Actividades que está guardados en el archivo
     *
     * @param Actividades Las Actividades
     * @return Todos las Actividades que estan el archivo guardados
     */
    public String impActividad(Actividad[] Actividades) {
        String msj = "";
        String formato = "%d. %s : %s %s %s %s %s\n";

        for (int i = 0; i < Actividades.length; i++) {
            Actividad actividadInfoNueva = Actividades[i];
            if (actividadInfoNueva == null) {
                return msj;
            }

             msj = String.format(formato, (i + 1),
                        "Tipo de Actividad: " + actividadInfoNueva.getTipoActividad(), "\n" + "Telefono: " + actividadInfoNueva.getTelefono()+ "\n",  "correo: " + actividadInfoNueva.getCorreo() + "\n" + "\n", "\n", "Fecha actividad: " + actividadInfoNueva.getFechaActividadString(),
                        "Hora: " + actividadInfoNueva.getHora()+ "\n", actividadInfoNueva.getCedulaContacto());
        }
        return msj;
    }

//    /**
//     * Calcula la edad de un Conctacto según la fecha de nacimiento
//     *
//     * @param fecha Fecha digitada por el Usuario
//     * @return La edad del Usuario
//     */
//    private int calcEdad(String fecha) {
//        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//        LocalDate fecNan = LocalDate.parse(fecha, dtf);
//        LocalDate hoy = LocalDate.now();
//
//        Period periodo = Period.between(fecNan, hoy);
////        System.out.printf("%d años, %d meses, %d días",
////                periodo.getYears(), periodo.getMonths(), periodo.getDays());
//
//        return periodo.getYears();
//    }

    /**
     * Edita Información del Actividad.
     *
     * @param info Información vieja.
     * @param temp Información Nueva.
     */
    public void editarActividad(String infoActividadVieja, Actividad actividaEditada) {
        String datos = ma.leer(RUTA_ACTIVID);
        datos = datos.replaceAll(infoActividadVieja, actividaEditada.getInfoActividad().trim());
        ma.escribir(RUTA_ACTIVID, datos, false);
    }

    
    

    /**
     * Edita Información del Actividad.
     *
     * @param info Información vieja.
     * @param temp Información Nueva.
     */
    public void eliminarActividad(String infoActividadVieja) {
        String datos = ma.leer(RUTA_ACTIVID);
        datos = datos.replaceAll(infoActividadVieja + "\n", "").trim();
        ma.escribir(RUTA_ACTIVID, datos, false);
    }
    /**
     * 
     * @param horaScreen
     * @return 
     */
    public boolean verificarHoraActividad(String horaScreen){
        Actividad[] listaActividades = leerActividad(RUTA_ACTIVID);
        Actividad verificarHoraEnActividad = new Actividad();
        for (int i = 0; i < listaActividades.length; i++) {
            verificarHoraEnActividad = listaActividades[i];
             String hora = verificarHoraEnActividad.getHora();
            if(hora.equals(horaScreen)){
               return true;
            }
        }
        return false;
}
    /**
     * Verifica Si la Fecha que ingresa el Usuario es un dia festivo o no 
     * @param fechaScreen fecha ingresada por el usuario
     * @return si la fecha es un dia festivo retorna true y en caso contrario
     * retorna false 
     */
    public boolean verificarDiaFestivo(String fechaScreen){
        String[] dia = {"11/04/2019","18/04/2019","19/04/2019","01/05/2019","25/07/2019","02/08/2019","15/08/2019","15/09/2019","14/10/2019","25/12/2019"};
        String verificarDiaFestivo = new String(); 
        for (int i = 0; i < dia.length; i++) {
            verificarDiaFestivo = dia[i];
            String diaFestivo = verificarDiaFestivo;
            if(diaFestivo.equals(fechaScreen)){
               return true;
            }
        }
        return false;
}
}





