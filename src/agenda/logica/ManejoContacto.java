/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda.logica;

import agenda.dao.ArchivoDao;
import agenda.datos.Contacto;
import agenda.datos.Usuario;
import agenda.util.Util;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author dell
 */
public class ManejoContacto {

    ArchivoDao ma = new ArchivoDao();
    private static final String RUTA_CONTACT = "datos/Contactos.txt";

    /**
     * Escribe el contacto nuevo en la ruta del archivo
     *
     * @param nueva Contacto nuevo
     */
    public void guardarContacto(Contacto nueva) {
        ma.escribir(RUTA_CONTACT, nueva.getInfoContacto().trim());
    }

    /**
     * lee todos los Usuarios del archivo
     *
     * @param rutaAchivo La ruta del donde se encuentra el archivo
     * @return La cantidad de usuarios que hay en el archivo
     */
    public Contacto[] leerContacto(String rutaAchivo) {
        String[] contactos = ma.leer(RUTA_CONTACT).split("\n");
        Contacto[] arr = new Contacto[contactos.length];
        for (int i = 0; i < contactos.length; i++) {
            String[] datoContacto = contactos[i].split(",");
            Contacto contacto = new Contacto();
            contacto.setCedula(datoContacto[0]);
            contacto.setNombre(datoContacto[1]);
            contacto.setFechaNacimiento(Util.convertirFecha(datoContacto[2]));
            contacto.setCorreo(datoContacto[3]);
            contacto.setTelefono(datoContacto[4]);
            contacto.setDescripcion(datoContacto[5]);
            contacto.setCedulaUser(datoContacto[6]);

            arr[i] = contacto;

        }
        return arr;
    }

    /**
     * Recorre la lista de Contacto para saber si ese Contacto existe y si no
     * los escribe en el archivo
     *
     * @param listaContacto Lista de Contacto nueva
     */
    public void guardarContacto(Usuario[] listaContacto) {

        for (int i = 0; listaContacto.length < 10; i++) {
            guardarContacto(listaContacto);
        }
    }

    /**
     * Verifica Usuario por numero de cedula para saber su información
     *
     * @param ced Cedula digitada por el usuario
     * @return Informacion del Usuario
     */
    public String impContacto(String ced) {
        Contacto[] listaContacto = leerContacto(RUTA_CONTACT);
        Contacto verificar = new Contacto();
        String msj = "";
        for (int i = 0; i < listaContacto.length; i++) {
            verificar = listaContacto[i];
            String cedula = verificar.getCedulaUser();
            if (cedula.equals(ced)) {
                String formato = " %d. %s : %s %s %s %s %s \n";
                Contacto contact = listaContacto[i];
                int edad = calcEdad(contact.getFechaNacimientoString());

                msj = String.format(formato, (i + 1),
                        "Nombre: " + contact.getNombre(), "\n" + "Cedula: " + contact.getCedula() + "\n", "Edad: " + edad + " años" + "\n", "correo: " + contact.getCorreo() + "\n",
                        "Telefono: " + contact.getTelefono() + "\n", "descripción: " + contact.getDescripcion() + "\n");
            }
        }
        return msj;

    }
    /**
     * 
     * @param ced
     * @param listaContactos
     * @return 
     */
    public String impContacto(String ced, Contacto[] listaContactos) {
        Contacto verificar = new Contacto();
        String msj = "";
        for (int i = 0; i < listaContactos.length; i++) {
            verificar = listaContactos[i];
            String cedula = verificar.getCedulaUser();
            if (cedula.equals(ced)) {
                String formato = " %d. %s : %s %s %s %s %s \n";
                Contacto contact = listaContactos[i];
                int edad = calcEdad(contact.getFechaNacimientoString());

                msj += String.format(formato, (i + 1),
                        "Nombre: " + contact.getNombre(), "\n" + "Cedula: " + contact.getCedula() + "\n", "Edad: " + edad + " años" + "\n", "correo: " + contact.getCorreo() + "\n",
                        "Telefono: " + contact.getTelefono() + "\n", "descripción: " + contact.getDescripcion() + "\n");

            }
        }
        return msj;

    }
    
    /**
     * 
     * @param ced
     * @param listaContactos
     * @return 
     */
     public String imprimirContactos(String ced, Contacto[] listaContactos) {
        Contacto verificar = new Contacto();
        String msj = "";
        for (int i = 0; i < listaContactos.length; i++) {
            verificar = listaContactos[i];
            String cedula = verificar.getCedulaUser();
            if (cedula.equals(ced)) {
                String formato = " %d#\n %s %s\n";
                Contacto contact = listaContactos[i];
           
                int edad = calcEdad(contact.getFechaNacimientoString());

                msj += String.format(formato, (i + 1),
                        "Nombre: " + contact.getNombre(), "\n" + " Cedula: " + contact.getCedula());

            }
        }
        return msj;

    }
    
    
    
    

    /**
     * Imprime todos los usuarios que está guardados en el archivo
     *
     * @param usuarios Los usuarios
     * @return Todos los usuarios que estan el archivo guardados
     */
    public String impUsuarios(Contacto[] contactos) {
        String msj = "";
        String formato = "%d. %s : %s %s %s %s %s\n";

        for (int i = 0; i < contactos.length; i++) {
            Contacto contact = contactos[i];
            if (contact == null) {
                return msj;
            }
            int edad = calcEdad(contact.getFechaNacimientoString());

            msj = String.format(formato, (i + 1),
                    "Nombre: " + contact.getNombre(), "\n" + "Cedula: " + contact.getCedula() + "\n", "Edad: " + edad + " años" + "\n", "correo: " + contact.getCorreo() + "\n",
                    "Telefono: " + contact.getTelefono() + "\n", "descripción: " + contact.getDescripcion() + "\n");
        }
        return msj;
    }

    /**
     * Calcula la edad de un Conctacto según la fecha de nacimiento
     *
     * @param fecha Fecha digitada por el Usuario
     * @return La edad del Usuario
     */
    private int calcEdad(String fecha) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fecNan = LocalDate.parse(fecha, dtf);
        LocalDate hoy = LocalDate.now();

        Period periodo = Period.between(fecNan, hoy);
//        System.out.printf("%d años, %d meses, %d días",
//                periodo.getYears(), periodo.getMonths(), periodo.getDays());

        return periodo.getYears();
    }

    /**
     * Edita Información del Usuario.
     *
     * @param info Información vieja.
     * @param temp Información Nueva.
     */
    public void editarContacto(String infoContactoVieja, Contacto contactoEditada) {
        String datos = ma.leer(RUTA_CONTACT);
        datos = datos.replaceAll(infoContactoVieja, contactoEditada.getInfoContacto().trim());
        ma.escribir(RUTA_CONTACT, datos, false);
    }

    /**
     * Edita Información del Usuario.
     *
     * @param info Información vieja.
     * @param temp Información Nueva.
     */
    public void eliminarContacto(String infoContactoVieja) {
        String datos = ma.leer(RUTA_CONTACT);
        datos = datos.replaceAll(infoContactoVieja + "\n", "").trim();
        ma.escribir(RUTA_CONTACT, datos, false);
    }
        
}
