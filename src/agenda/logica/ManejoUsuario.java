/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda.logica;

import agenda.dao.ArchivoDao;
import agenda.datos.Usuario;
import agenda.util.Util;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author dell
 */
public class ManejoUsuario {
    ArchivoDao ma  = new ArchivoDao();
    private static final String RUTA_USER = "datos/Ussuario.txt";
    
    /**
     * Escribe el usuario nuevo en la ruta del archivo
     * @param nueva Usuario nuevo 
     */
    public void guardarUsuario(Usuario nueva){
        ma.escribir(RUTA_USER, nueva.getInfoUsuario());

    }
    /**
     * lee todos los Usuarios del archivo
     * @param rutaAchivo La ruta del donde se encuentra el archivo
     * @return La cantidad de usuarios que hay en el archivo
     */
    public Usuario[] leerUsuarios(String rutaAchivo){
        String[] usuarios = ma.leer(RUTA_USER).split("\n");
        Usuario[] arr = new Usuario[usuarios.length];
        for (int i = 0; i < usuarios.length; i++) {
            String[] datoUsuario = usuarios[i].split(",");
            Usuario usuario = new Usuario();
            usuario.setNombre(datoUsuario[0]);
            usuario.setCorreo(datoUsuario[1]);
            usuario.setCedula(datoUsuario[2]);
            usuario.setFechaNacimiento(Util.convertirFecha(datoUsuario[3]));
            usuario.setPassword(datoUsuario[4]);
            usuario.setUserName(datoUsuario[5]);
            usuario.setPhone(datoUsuario[6]);
            arr[i] = usuario;
      
       
    }
        return arr;
    }
    /**
     * Recorre la lista de Usuarios para saber si ese usuario existe y si no los
     * escribe en el archivo
     * @param listaUsuarios Lista de Usuario nueva
     */
    public void guardarUsuario(Usuario[] listaUsuarios ){
        
        for (int i = 0; listaUsuarios.length < 10; i++) {
            guardarUsuario(listaUsuarios);
        }
    }
    /**
     * Lee la ruta de Usuarios la recorre para verificar si los datos contraseña
     * y nombre de usuario son validos y existen.
     * @param usernameScreen Nombre de Usuario que digita el Usuario en pantalla.
     * @param passwordScreen Contraseña que digita el Usuario en pantalla.
     * @return devulve true, si la contraseña y nombre de usuario existen en 
     * el archivo de resgistro de usuario y false en caso contrario.
     */
    public boolean verificarAcceso(String usernameScreen, String passwordScreen){
        Usuario[] listaUsuarios = leerUsuarios(RUTA_USER);
        Usuario verificar = new Usuario();
        for (int i = 0; i < listaUsuarios.length; i++) {
            verificar = listaUsuarios[i];
            String password = verificar.getPassword();
            String userName = verificar.getUserName();
            if(userName.equals(usernameScreen)&& (password.equals(passwordScreen))){
               return true;
            }
        }
        return false;
    }
    
    
    
    
    /**
     * Lee la ruta de Usuarios la recorre para verificar si los datos contraseña
     * y nombre de usuario son validos y existen.
     * @param usernameScreen Nombre de Usuario que digita el Usuario en pantalla.
     * @param passwordScreen Contraseña que digita el Usuario en pantalla.
     * @return devulve true, si la contraseña y nombre de usuario existen en 
     * el archivo de resgistro de usuario y false en caso contrario.
     */
    public boolean verificarUsuario(String usernameScreen,String cedulaScreen){
        Usuario[] listaUsuarios = leerUsuarios(RUTA_USER);
        Usuario verificar = new Usuario();
        for (int i = 0; i < listaUsuarios.length; i++) {
            verificar = listaUsuarios[i];
             String cedula = verificar.getCedula();
            String userName = verificar.getUserName();
            if(userName.equals(usernameScreen)|| (cedula.equals(cedulaScreen))){
               return true;
            }
        }
        return false;
    }
    /**
     * Imprime todos los usuarios que está guardados en el archivo
     * @param usuarios Los usuarios 
     * @return Todos los usuarios que estan el archivo guardados
     */
    public String impUsuarios(Usuario[] usuarios) {
        String msj = "";
        String formato = "%d. %s : %s, %s,%s,%s,%s,%s\n";

        for (int i = 0; i < usuarios.length; i++) {
            Usuario m = usuarios[i];
            if (m == null) {
                return msj;
            }
            int edad = calcEdad(m.getFechaNacimientoString());
            
            msj += String.format(formato, (i + 1),
                    m.getNombre(), m.getCorreo(),m.getCedula(),
                     edad, m.getPassword(),m.getUserName(),m.getPhone() );
        }
        return msj;
    }
    /**
     * Busca usuario por su numero de cedula
     * @param ced cedula digitada por el usuario
     * @return Info del usuario
     */
    public Usuario buscarUsuario(String ced){
        Usuario[] listaUsuarios = leerUsuarios(RUTA_USER);
        Usuario verificar = new Usuario();
        for (int i = 0; i < listaUsuarios.length; i++) {
            verificar = listaUsuarios[i];
            String cedula = verificar.getCedula();
            if(cedula.equals(ced)){
                String msj = "";
                String formato = " %d. %s : %s %s %s %s %s %s\n";
                    Usuario user = listaUsuarios[i];
                   
                return user;
            }
        }
        return null;
    }
    
     public Usuario buscarNombreUsuario(String userScream){
        Usuario[] listaNombreUser = leerUsuarios(RUTA_USER);
        Usuario verificar = new Usuario();
        for (int i = 0; i < listaNombreUser.length; i++) {
            verificar = listaNombreUser[i];
            String nombreUser = verificar.getUserName();
            if(nombreUser.equals(userScream)){
                Usuario user = listaNombreUser[i];
                return user;
            }
    }
        return null;
    }
    
    
    /**
     * Verifica Usuario por numero de cedula para saber su información
     * @param ced Cedula digitada por el usuario
     * @return Informacion del Usuario
     */
    public String impUsuario(String ced){
        Usuario[] listaUsuarios = leerUsuarios(RUTA_USER);
        Usuario verificar = new Usuario();
        for (int i = 0; i < listaUsuarios.length; i++) {
            verificar = listaUsuarios[i];
            String cedula = verificar.getCedula();
            if(cedula.equals(ced)){
                String msj = "";
                String formato = " %d. %s : %s %s %s %s %s %s\n";
                    Usuario user = listaUsuarios[i];
                    int edad = calcEdad(user.getFechaNacimientoString());
                    
                    msj = String.format(formato,(i + 1),
                    user.getNombre(), "\nCorreo: "+ user.getCorreo()+"\n","Cedula: "+ user.getCedula()+"\n",
                     "Edad: "+ edad+" años" +"\n","Contraseña: "+ user.getPassword()+"\n","Usuario: "+ user.getUserName()+"\n","Telefono: "+ user.getPhone()+"\n");
                
                return msj;
            }
        }
        return null;
    
    }
    
    
    /**
     * Calcula la edad de un Usuario según la fecha de nacimiento
     * @param fecha Fecha digitada por el Usuario
     * @return La edad del Usuario
     */
    private int calcEdad(String fecha) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fecNan = LocalDate.parse(fecha, dtf);
        LocalDate hoy = LocalDate.now();

        Period periodo = Period.between(fecNan, hoy);
//        System.out.printf("%d años, %d meses, %d días",
//                periodo.getYears(), periodo.getMonths(), periodo.getDays());

        return periodo.getYears();
    }
    /**
     * Edita Información del Usuario.
     * @param info Información vieja.
     * @param temp Información Nueva.
     */
    public void editarUsuario(String info, Usuario temp) {
        String datos = ma.leer(RUTA_USER);
        datos = datos.replaceAll(info, temp.getInfoUsuario().trim());
        ma.escribir(RUTA_USER, datos, false);
    }
    /**
     * Crea un aleatorio de contraseña
     * @return La contraseña nueva del usuario 
     */
    public String passRandom(){
        
        //int max = 10;
        int min = 1;
        int rango = 10;
        String pass = "";
        
        for (int i = 0; i <= 4; i++) {
           
         String numeroRandom   =  String.valueOf((int) (Math.random()*rango)+min) ;
         pass = pass + numeroRandom;         
        }           
        return pass;
        
    }
    // Aqui incia metodos Contactos y Actividades quedan pendientes


    //Actividades
    
    
   
}
    
        

